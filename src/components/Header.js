import React from "react";

// Import of react bootsrap for designing
import { Container, Row, Col } from "react-bootstrap";

import "../styles/header.css";

export default function Header({ title }) {
  return (
    <Container fluid className="app-navbar  m-0">
      <Row className="app-navbar-row  p-0 m-0 ">
        <Col className="app-navbar-col  p-0 m-0  d-flex justify-content-center align-items-center">
          <div className="app-navbar-logo  d-flex justify-content-center align-items-center p-0 m-0 me-3">
            <h1 className="d-flex align-items-center">
              H<span className="mt-1"></span>
            </h1>
          </div>
          <div className="d-flex align-items-center ">
            <h1 className="navbar-title">{title}</h1>
          </div>
        </Col>
      </Row>
    </Container>
  );
}
