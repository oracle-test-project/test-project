import React from "react";

import "../styles/ContactNumber.css";
import { Container, Row, Col } from "react-bootstrap";

export default function ContactNumberTable({
  tblName,
  Name,
  Mobile,
  Email,
  data,
}) {
  return (
    <Container fluid className="contact-number-tbl  m-0">
      <Row className="contact-number-tbl-row   p-1 m-0">
        <Col className="contact-number-tbl-col  p-1 m-0">
          <h1>{tblName}</h1>
          <table className="contact-list-tbl ">
            <thead>
              <tr className="tbl-head">
                <th>{Name}</th>
                <th>{Mobile}</th>
                <th>{Email}</th>
              </tr>
            </thead>

            <tbody>
              {data.map((item, index) => (
                <tr key={index} className="tbl-body ">
                  <td>{item.name}</td>
                  <td>{item.mobile}</td>
                  <td>{item.email}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </Col>
      </Row>
    </Container>
  );
}
