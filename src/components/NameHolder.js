import "../styles/NameHolder.css";

import React from "react";

export default function NameHolder({ name }) {
  return (
    <div className={`name-holder fade-in`}>
      <ul className="m-0 ">
        <li>{name}</li>
      </ul>
    </div>
  );
}
