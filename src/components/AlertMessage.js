import React from "react";

import "../styles/AlertMessage.css";
import { Col, Container, Row } from "react-bootstrap";

export default function AlertMessage(iconUrl, errorMsg) {
  return (
    <Container fluid className="alert-msg">
      <Row className="alert-msg-row ">
        <Col className="alert-msg-col  px-3 m-0">
          <img
            className="warning-icon me-auto "
            src="https://cdn-icons-png.flaticon.com/512/8434/8434518.png"
          />
          <h3 className="me-auto m-0">Input Invalid</h3>
        </Col>
      </Row>
    </Container>
  );
}
