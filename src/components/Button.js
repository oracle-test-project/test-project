import React from "react";
import "../styles/Button.css";

export default function Button({ text, onClick }) {
  return <button onClick={onClick}>{text}</button>;
}
