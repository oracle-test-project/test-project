import "../styles/SearchField.css";

import { useState, useEffect } from "react";

import React from "react";

export default function SearchField({ value, placeHolder, onInputChange }) {
  const [inputFieldValue, setInputFieldValue] = useState("");

  const handleInputValue = (event) => {
    const newValue = event.target.value;
    setInputFieldValue(newValue);

    onInputChange(newValue);
  };

  return (
    <input
      id="add-customer"
      className="add-custoner-field"
      type="text"
      value={value}
      placeholder={placeHolder}
      onChange={handleInputValue}
    ></input>
  );
}
