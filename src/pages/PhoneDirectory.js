// import React libraries
import { Container, Row, Col } from "react-bootstrap";
import { React, useState } from "react";

// import specific CSS for PhoneDirectory
import "../styles/PhoneDirectory.css";

// import components to be used in this PhoeDirectory Page from components folder
import InputField from "../components/SearchField.js";
import AddButton from "../components/Button.js";
import AlertMessage from "../components/AlertMessage";
import ContactNumberTable from "../components/ContactNumberTable";

export default function PhoneDirectory() {
  // useState to get the value of input fields
  const [nameInput, setNameInput] = useState("");
  const [mobileInput, setMobileInput] = useState("");
  const [emailInput, setEmailInput] = useState("");

  // useState that handle/store data temporarily
  const [dataContainer, setDataContainer] = useState([{}]);
  const [isFirstClick, setIsFirstClick] = useState(true);
  const [isInvalidInput, setIsInvalidInput] = useState(false);

  // function to handle the setting/getting the value input field for name
  const getNameInput = (data) => {
    setNameInput(data);
  };
  // function to handle the setting/getting the value input field for mobile
  const getMobileInput = (data) => {
    setMobileInput(data);
  };
  // function to handle the setting/getting the value input field email
  const getEmailInput = (data) => {
    setEmailInput(data);
  };

  // this function is for button click behaviour
  const handleOnClick = () => {
    // These declaration store the regex of criteria to be checked based on given criteria on docx file
    const nameInputed = /^[A-Za-z\s]*$/;
    const mobileInputed = /^[0-9]*$/;
    const emailInputed =
      /^[A-Za-z][A-Za-z0-9.]{2,10}@[A-Za-z]{2,20}.[A-Za-z]{2,10}/;

    // Logic for checking input criteria
    if (
      // Check for string and space with <= 20 limit on character
      typeof nameInput === "string" &&
      nameInputed.test(nameInput) &&
      nameInput.length - 1 <= 20 &&
      nameInput !== "" &&
      // Check for mobile input
      mobileInput !== "" &&
      mobileInputed.test(mobileInput) &&
      mobileInput.length === 10 &&
      // Check for email input
      emailInputed.test(emailInput) &&
      emailInput !== ""
    ) {
      // Schema for data to be passed on data container
      const inputData = {
        name: nameInput,
        mobile: mobileInput,
        email: emailInput,
      };
      // This will determine if it is the first time to push data on the array so it will replace the old value
      if (isFirstClick) {
        // On first click, replace the empty object at index 0
        setDataContainer([inputData]);
        setIsFirstClick(false); // Set isFirstClick to false
      } else {
        // if the it was not first time to push data this will happen which will combine the value or it will opush/append new data on data container
        setDataContainer((originalContainer) => [
          ...originalContainer,
          inputData,
        ]);
      }
      // this will set to default of false to so that if ever you enter wrong data on first click the error message will hide
      setIsInvalidInput(false);

      // clear the input filed
      setNameInput("");
      setMobileInput("");
      setEmailInput("");
    } else {
      // this will now catch what will heppen if invalid input which will make the error messag show
      setIsInvalidInput(true);
    }
  };

  return (
    // this is the page design where i invoke the components:
    // each components has its own props which will be inherited here for getting the data and setting the data to be displayed on web page
    /* Props:
      placholder = this is for setting the default value of the input field
      value = this if for target value of input field
      onInputChange = this is for getting the value of the input field from user to be store on arrays/container
      =======
      tblName= sets the table name of the table     
      Name = this will display in table header for name
      Mobile= this will display in table  header for mobile
      Email= this will display in table header for email
      data=is a porps that holds the value of container to be passed on table compoennts
      onClick = this will get the function of handleClick to execute the function
      */
    <Container fluid className="phone-directory  p-0 m-0">
      <Row className="phone-directory-row  p-1 m-0">
        <Col className="phone-directory-col  p-1 m-0">
          <div className="input-field-section  mb-1 d-flex justify-content-evenly align-items-center">
            <InputField
              placeHolder="Contact Name"
              value={nameInput}
              onInputChange={getNameInput}
            />
            <InputField
              placeHolder="Mobile Number"
              value={mobileInput}
              onInputChange={getMobileInput}
            />
            <InputField
              placeHolder="Email"
              value={emailInput}
              onInputChange={getEmailInput}
            />
            <AddButton text="Add Number" onClick={handleOnClick} />
          </div>
          <div
            className={`error-massage-section mb-1 d-flex justify-content-center align-items-center p-3 ${
              isInvalidInput ? "" : "d-none"
            }`}
          >
            <AlertMessage />
          </div>
          <div className="display-directory-section pt-3  d-flex flex-column justify-content-start align-items-center">
            <ContactNumberTable
              tblName="Contact Summary"
              Name="Name"
              Mobile="Mobile"
              Email="Email"
              data={dataContainer}
            />
          </div>
        </Col>
      </Row>
    </Container>
  );
}
