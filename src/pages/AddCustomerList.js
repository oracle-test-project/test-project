import "../styles/addCustomer.css";
import { Container, Row, Col } from "react-bootstrap";
import React, { useState } from "react";

// component importation
import SearchField from "../components/SearchField.js";
import Button from "../components/Button.js";
import NameHolder from "../components/NameHolder.js";

export default function AddCustomerList() {
  // input field setting
  const [inputValue, setInputValue] = useState("");
  // data storing
  const [nameContainer, setNameContainer] = useState([]);

  // function for chaing inputValue data
  const handleInputField = (data) => {
    setInputValue(data);
  };

  // function for button clicked
  const handleClick = () => {
    if (inputValue === "") return;

    // Create a copy of the existing array and append inputValue to it
    const updatedNameContainer = [...nameContainer, inputValue];

    // Update the nameContainer state with the new values
    setNameContainer(updatedNameContainer);
    // Clear the inputField
    setInputValue("");
  };

  return (
    <Container fluid className="customer-list-container  p-0 m-0">
      <Row className="customer-list-row p-0 m-0">
        <Col className="customer-list-col p-0 m-0">
          <div className="add-customer  d-flex  align-items-center justify-content-evenly">
            <div className="text-field ">
              <SearchField
                value={inputValue}
                placeHolder="Name"
                onInputChange={handleInputField}
              />
            </div>
            <div className="add-btn ">
              <Button onClick={handleClick} text="Add Customer" />
            </div>
          </div>
          <div className="display-customer   d-flex flex-column align-items-center  p-5 ">
            {nameContainer.map((name, index) => (
              <NameHolder className={"fade-in"} key={index} name={name} />
            ))}
          </div>
        </Col>
      </Row>
    </Container>
  );
}
