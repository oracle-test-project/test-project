import { Container, Row, Col } from "react-bootstrap";

import { Link } from "react-router-dom";

export default function PageNotFound() {
  return (
    <Container className=" container text-center">
      <Row>
        <Col className=" mx-auto">
          <h2>Page Not Found</h2>
          <img
            className="col-8 mx-auto"
            src="https://cdn.svgator.com/images/2022/01/404-page-animation-example.gif"
          />
          <p>
            Go back to the <Link to="/product">product</Link>
          </p>
        </Col>
      </Row>
    </Container>
  );
}
