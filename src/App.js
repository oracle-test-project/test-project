import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import React, { useState, useEffect } from "react";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";

// importation of
import Header from "./components/Header.js";
import AddCustomerList from "./pages/AddCustomerList.js";
import PhoneDirectory from "./pages/PhoneDirectory.js";
import PageNotFound from "./pages/PageNotFound";

function App() {
  // Determine the current route from location.pathname
  let currentRoute = "";
  if (location.pathname === "/AddCustomerList") {
    currentRoute = "Customer List";
  } else if (location.pathname === "/AddPhoneList") {
    currentRoute = "Phone Directory";
  } else if (location.pathname === "/") {
    currentRoute = "Customer List";
  } else {
    currentRoute = "";
  }
  return (
    <BrowserRouter>
      <Header title={currentRoute} />
      <Routes>
        <Route path="/AddCustomerList" element={<AddCustomerList />} />
        <Route path="/AddPhoneList" element={<PhoneDirectory />} />
        <Route path="/notAccessible" element={<PageNotFound />} />
        <Route path="*" element={<Navigate to="/notAccessible" />} />
        <Route path="/" element={<Navigate to="/AddCustomerList" />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
